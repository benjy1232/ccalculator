#include<stdio.h>
#include<math.h>

int main(void)
{
    printf("Please enter a selection\n");
    printf("0 to exit\n");
    printf("1 for addition\n");
    printf("2 for subtraction\n");
    printf("3 for multiplication\n");
    printf("4 for division\n");
    printf("5 for factorial\n");
    printf("6 for exponential\n");
    printf("7 for log base 10\n");
    printf("8 for natural log\n");
    printf("9 for a log with base chosen by you\n");
    printf("10 for a square root\n");
    printf("<++>\n");
    printf("<++>\n");
    printf("<++>\n");
    printf("<++>\n");
    printf("<++>\n");
    printf("<++>\n");
    printf("<++>\n");
}